var express = require('express');
var mongoose = require('mongoose');
var mime = require('mime');
var app = express();
var fs = require('fs');
var path = require('path');
mongoose.connect('mongodb://localhost/test');

/* Init */
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	/* We're connected! */
	app.listen(4001, function () {
		console.log('API listening at localhost:4001');
	});
});

/* Setting up origin headers */
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4000');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	next();
});

/* Preparing data models */
var imageSchema = mongoose.Schema({
	index: String,
	size: Number,
	access_time: Date,
	modified_time: Date,
	change_time: Date,
	birth_time: Date,
	mime_type: String
});

var Image = mongoose.model(
	'Image',
	imageSchema);

/* Returns all images
   Acceps two query parameters: page and limit
   Limit indicates how many items should be returned maximum
   Page indicates which page should be returned */
app.get('/images', (req, res) => {
	var limit = Number(req.query.limit) || 50;
	var page = Number(req.query.page) || 1;
	var skip = (page - 1) * limit;
	var searchValues = { };
	if(req.query.searchString) searchValues = { 'index': new RegExp(req.query.searchString, 'i') };
	if(req.query.mimeType) searchValues.mime_type = req.query.mimeType;
	Image.count(searchValues, (err, count) => {
		if(err) {
			res.status(500).send(err);
		} else {
			Image.find(searchValues, {}, { skip: skip, limit: limit }, (err, images) => {
				if(err) {
					res.status(500).send(err);
				} else {
					res.send({ count: count, images: images });
				}
			});
		}
	});
});

/* Returns an image file for the given id */
app.get('/images/:id', (req, res) => {
	Image.findById(req.params.id, (err, image) => {
		if(err) res.status(404).send('NOT FOUND');
		else res.sendFile(image.index);
	});
});

/* Post function that takes care of scanning the file system and adding all
   images to the database. */
app.post('/images', (req, res) => {
	var path = process.env.HOME;
	if(process.argv[2]) path = process.argv[2];
	walk(path, (err, results) => {
		/* Walk finished, inserting images */
		Image.insertMany(results, (err, insertResults) => {
			if(err) {
				res.status(500).send(err);
			}
			else res.status(201).send();
		});
	});
});

/* Delete the image collection */
app.delete('/images', (req, res) => {
	Image.remove({}, (err, results) => {
		if(err) res.status(500).send();
		else res.status(200).send();
	});
});

/* Helper function that takes care of walking through all
   directories starting from dir, finding all images and
   returning them */
var walk = function(dir, done) {
	var results = [];
	fs.readdir(dir, (err, list) => {
		if (err) return done(err);
		var pending = list.length;
		if (!pending) return done(null, results);
		list.forEach(index => {
			index = path.resolve(dir, index);
			/* Getting statistics about the file */
			fs.stat(index, (err, stat) => {
				/* If it's a directory we keep on going */
				if (stat && stat.isDirectory()) {
					walk(index, (err, res) => {
						results = results.concat(res);
						if (!--pending) done(null, results);
					});
				} else {
					/* Looking up the mime type of this file */
					var m = mime.lookup(index);
					if(m.indexOf('image') > -1) {
						/* Creating the model */
						var img = {
							index: index,
							size: stat.size,
							access_time: stat.atime,
							modified_time: stat.mtime,
							change_time: stat.ctime,
							birth_time: stat.birthtime,
							mime_type: m
						};
						results.push(img);
					}
					if (!--pending) done(null, results);
				}
			});
		});
	});
};

module.exports = this;