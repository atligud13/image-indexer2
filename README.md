# Image Indexer

Uses the following dependancies:
- [React](https://github.com/facebook/react)
- [Babel 6](http://babeljs.io)
- [Webpack](http://webpack.github.io) for bundling
- [Webpack Dev Server](http://webpack.github.io/docs/webpack-dev-server.html)
- [React Transform](https://github.com/gaearon/react-transform-hmr) for hot reloading React components in real time.

The various webpack options used have been explained in detailed as comments in the config file.


### MongoDB

```
Download MongoDB from https://www.mongodb.com
Go to the MongoDB bin folder
Example: C:\Program Files\MongoDB\Server\3.2\bin
mongod --dbpath C:\path\to\data\folder (this is a random empty folder where you choose to put your data, it must exist)
Wait for [initandlisten] and you're good to go! Keep it running while the application runs.
```

### Usage

```
npm install
npm start
Open http://localhost:4000
```

### Command line arguments
```
You can also supply a relative path to npm start as a command line argument
If you don't want the process to start scanning from the HOME folder. Example:
npm start "../../"
```