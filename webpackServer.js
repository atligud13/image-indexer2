/* eslint-disable no-var, strict, prefer-arrow-callback */
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');

new WebpackDevServer(webpack(config), {
	publicPath: config.output.publicPath,
	hot: true,
	historyApiFallback: true
}).listen(4000, 'localhost', function (err) {
	if (err) {
		console.log(err);
	}
	console.log('Client listening at localhost:4000');
});

module.exports = this;

