/* This file serves as an entry point to the application.
   This way I can fire up the webpack server and the api server together */

var webPackServer = require('./webpackServer');
var apiServer = require('./apiServer');