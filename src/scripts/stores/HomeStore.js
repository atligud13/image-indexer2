import alt from 'MyAlt';
import { createStore, bind } from 'alt-utils/lib/decorators';
import HomeActions from 'actions/HomeActions';
import ImageAPI from 'api/ImageAPI';

@createStore(alt)
class HomeStore {

	constructor() {
		this.limit = 15;
		this.page = 1;
		this.searchString = '';
		this.mimeType = '';
		this.images = [];
		this.activeImage = '';
		this.errorLoadingImage = false;
		this.errorLoadingImages = false;
		this.loadingImage = false;
		this.loadingImages = false;
		this.refreshingDatabase = false;
		this.count = 0;
	}

	/* (page) */
	@bind(HomeActions.getImages)
	getImages() {
		ImageAPI.getImages(this.page, this.limit, '', '', (err, res) => {
			if(err) {
				this.setState({ loadingError: true });
			}
			else {
				const images = this.images.concat(res.body.images);
				this.setState({ images: images, loadingImages: false, count: res.body.count });
				this.emitChange();
			}
		});
	}

	@bind(HomeActions.getImage)
	getImage(id) {
		this.setState({ loadingImage: true, errorLoadingImage: false });
		ImageAPI.getImage(id, (err, res) => {
			if(err) this.setState({ errorLoadingImage: true, loadingImage: false });
			else this.setState({ activeImage: res.body, loadingImage: false });
		});
	}

	@bind(HomeActions.searchImages)
	searchImages(value) {
		this.setState({ loadingImages: true, errorLoadingImages: false, searchString: value, page: 1 });
		ImageAPI.getImages(this.page, this.limit, value, '', (err, res) => {
			if(err) this.setState({ errorLoadingImages: true, loadingImages: false });
			else this.setState({ images: res.body.images, loadingImages: false, count: res.body.count });
		});
	}

	@bind(HomeActions.searchMimeType)
	searchMimeType(value) {
		this.setState({ loadingImages: true, errorLoadingImages: false, mimeType: value, page: 1 });
		ImageAPI.getImages(this.page, this.limit, '', value, (err, res) => {
			if(err) this.setState({ errorLoadingImages: true, loadingImages: false });
			else { 
				this.setState({ images: res.body.images, loadingImages: false, count: res.body.count });
			}
		});
	}

	@bind(HomeActions.refreshDatabase)
	refreshDatabase() {
		this.setState({ images: [], refreshingDatabase: true, errorLoadingImages: false, page: 1, count: 0 });
		ImageAPI.refreshDatabase((err, res) => {
			if(err) this.setState({ errorLoadingImages: true, loadingImages: false });
			else {
				this.setState({ loadingImages: true, refreshingDatabase: false });
				this.getImages();
			}
		});
	}

	@bind(HomeActions.dropDatabase)
	dropDatabase() {
		this.setState({ images: [], loadingImages: true, errorLoadingImages: false, page: 1, count: 0 });
		ImageAPI.dropDatabase((err, res) => {
			if(err) this.setState({ errorLoadingImages: true, loadingImages: false });
			else this.setState({ loadingImages: false });
		});
	}

	@bind(HomeActions.getPreviousPage)
	getPreviousPage() {
		if(this.page > 1) {
			this.page--;
			ImageAPI.getImages(this.page, this.limit, this.searchString, this.mimeType, (err, res) => {
				if(err) this.setState({ errorLoadingImages: true, loadingImages: false });
				else this.setState({ images: res.body.images, loadingImages: false });
			});
		}
	}

	@bind(HomeActions.getNextPage)
	getNextPage() {
		this.page++;
		ImageAPI.getImages(this.page, this.limit, this.searchString, this.mimeType, (err, res) => {
			if(err) this.setState({ errorLoadingImages: true, loadingImages: false });
			else this.setState({ images: res.body.images, loadingImages: false });
		});
	}

	@bind(HomeActions.clearActiveImage)
	clearActiveImage() {
		this.setState({ activeImage: '' });
	}
}

export default HomeStore;
