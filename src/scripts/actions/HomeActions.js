import alt from 'MyAlt';
import { createActions } from 'alt-utils/lib/decorators';

@createActions(alt)
class HomeActions {
	constructor() {
		this.generateActions('getImages');
		this.generateActions('getImage');
		this.generateActions('searchImages');
		this.generateActions('searchMimeType');
		this.generateActions('refreshDatabase');
		this.generateActions('dropDatabase');
		this.generateActions('getPreviousPage');
		this.generateActions('getNextPage');
		this.generateActions('clearActiveImage');
	}
}

export default HomeActions;
