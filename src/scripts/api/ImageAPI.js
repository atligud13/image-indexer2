import HttpUtil from 'utils/HttpUtil';

const BASE_URL = 'http://localhost:4001';
class ImageAPI {
	static getImages(page, limit, searchString, mimeType, cb) {
		let queryString = '';
		if(page && limit) queryString = '?page=' + page + '&limit=' + limit; 
		if(searchString) queryString = queryString.concat('&searchString=' + searchString);
		if(mimeType) queryString = queryString.concat('&mimeType=' + mimeType);
		HttpUtil.get(BASE_URL + '/images' + queryString, cb);
	}

	static getImage(id, cb) {
		HttpUtil.getImage(BASE_URL + '/images/' + id, cb);
	}

	static refreshDatabase(cb) {
		HttpUtil.post(BASE_URL + '/images', {}, cb);
	}

	static dropDatabase(cb) {
		HttpUtil.del(BASE_URL + '/images', cb);
	}
}

export default ImageAPI;
