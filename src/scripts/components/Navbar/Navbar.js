import React, { Component } from 'react';
import connectToStores from 'alt-utils/lib/connectToStores';
import { Link } from 'react-router';
import '../../../styles/modules/Navbar.scss';

class Navbar extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<header className="navigation">
				<div className="brand">
					<h2> Image indexer </h2>
				</div>
			</header>
		);
	}
}

export default Navbar;
