import React, { Component } from 'react';
import { RouterContext } from 'react-router';
import connectToStores from 'alt-utils/lib/connectToStores';
import Navbar from 'components/Navbar/Navbar';
import '../../styles/modules/Container.scss';

class Container extends Component {
	constructor(props, context) {
		super(props);
	}

	render() {
		return (
			<div id="container">
				<Navbar />
				<div className="app">
					{ this.props.children }
				</div>
			</div>
		);
	}
}

export default Container;
