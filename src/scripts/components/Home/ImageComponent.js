import React, { Component } from 'react';
import actions from 'actions/HomeActions';

class ImageComponent extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let imageSection = <img src={this.props.activeImage} className="img" placeholder="Unable to display image"/>;
		if(this.props.loadingImage) imageSection = <span className="img"> Loading image </span>;
		else if(this.props.errorLoadingImage) imageSection = <span className="img"> Error loading image </span>;
		return (
			<li>
				<span id={this.props.image._id} onClick={this.props.expandSection.bind(this, this.props.image.index)}>
					{this.props.image.index}
				</span>
				<ul className={'submenu' + (this.props.expanded === this.props.image._id ? ' is-expanded' : '')}>
					<li>
						<div className="flex-container">
							<div className="image-section">
								{imageSection}
							</div>
							<div className="info-section">
								<div className="info-rows">
									<div className="info-item"> Access time: {this.props.image.access_time} </div>
									<div className="info-item"> Birth time: {this.props.image.birth_time} </div>
									<div className="info-item"> Mime type: {this.props.image.mime_type} </div>
									<div className="info-item"> Change time: {this.props.image.change_time} </div>
									<div className="info-item"> Modified time: {this.props.image.modified_time} </div>
								</div>
							</div>
						</div>
						<div className="index"> 
							<strong> Full index: </strong> 
							<div className="scroll-wrapper"> {this.props.image.index} </div>
						</div>
					</li>
				</ul>
			</li>
		);
	}
}

export default ImageComponent;
