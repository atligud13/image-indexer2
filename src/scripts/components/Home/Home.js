import React, { Component } from 'react';
import connectToStores from 'alt-utils/lib/connectToStores';
import HomeStore from 'stores/HomeStore';
import ImageComponent from 'components/Home/ImageComponent';
import Filters from 'components/Home/Filters';
import Spinner from 'components/Loaders/Spinner';
import NoImagesFoundComponent from 'components/Home/NoImagesFoundComponent';
import actions from 'actions/HomeActions';
import '../../../styles/modules/Home.scss';

@connectToStores
class Home extends Component {
	constructor(props) {
		super(props);
		this.expandSection = this.expandSection.bind(this);
		this.getImageComponent = this.getImageComponent.bind(this);
		this.state = { expanded: '' };
	}

	componentWillMount() {
		actions.getImages();
	}

	static getStores(props) {
		return [HomeStore];
	}

	static getPropsFromStores(props) {
		return HomeStore.getState();
	}

	expandSection(index, e) {
		let id = e.target.id;
		if (this.state.expanded === id) id = '';
		this.setState({ expanded: id });
		/* Requesting the server for the image */
		if(id !== '' && index !== undefined) actions.getImage(id);
		if(index === undefined) actions.clearActiveImage();
	}

	getImageComponent(image) {
		return (
			<ImageComponent image={image} 
							expanded={this.state.expanded} 
							key={image._id} 
							expandSection={this.expandSection} 
							activeImage={this.props.activeImage}
							loadingImage={this.props.loadingImage}
							errorLoadingImage={this.props.errorLoadingImage} />
		);
	}

	render() {
		let accordionSection = (<ul className="accordion">
									{this.props.images.map(this.getImageComponent)}
								</ul>);
		if(this.props.images.length === 0) accordionSection = <NoImagesFoundComponent />;
		if(this.props.loadingImages) accordionSection = (<div className="spinner-wrapper"> 
															<Spinner />
															<span> Loading Images </span>
														</div>);
		if(this.props.refreshingDatabase) accordionSection = (<div className="spinner-wrapper"> 
																<Spinner />
																<span> Refreshing Database </span>
															</div>);
		if(this.props.errorLoadingImages) accordionSection = <span> Error loading images </span>;
		return (
			<div className="home">
				<Filters count={this.props.count} limit={this.props.limit} page={this.props.page} />
				{accordionSection}
			</div>
		);
	}
}

export default Home;
