import React, { Component } from 'react';
import actions from 'actions/HomeActions';
import '../../../styles/modules/Filters.scss';

class Filters extends Component {
	constructor(props) {
		super(props);
		this.state = { searchValue: '', selectedMime: '' };
		this.onSearchChange = this.onSearchChange.bind(this);
		this.onSearchButtonClick = this.onSearchButtonClick.bind(this);
		this.onSearchKeyDown = this.onSearchKeyDown.bind(this);
		this.onMimeTypeChange = this.onMimeTypeChange.bind(this);
	}

	onSearchKeyDown(e) {
		if(e.keyCode === 13) {
			actions.searchImages(e.target.value);
			this.setState({ searchValue: '' });
		}
	}

	onSearchButtonClick(e) {
		actions.searchImages(this.state.searchValue);
		this.setState({ searchValue: '' });
	}

	onSearchChange(e) {
		this.setState({ searchValue: e.target.value });
	}

	onMimeTypeChange(e) {
		this.setState({ selectedMime: e.target.value });
		actions.searchMimeType(e.target.value);
	}

	refreshDatabase() {
		actions.refreshDatabase();
	}

	dropDatabase() {
		actions.dropDatabase();
	}

	getPreviousPage() {
		actions.getPreviousPage();
	}

	getNextPage() {
		actions.getNextPage();
	}

	render() {
		const leftArrow = '<';
		const rightArrow = '>';
		return (
			<div className="flex-container">
				<div className="filters">
					<select className="mime-dropdown" value={this.state.selectedMime} onChange={this.onMimeTypeChange}>
						<option value=""> All mime types </option>
						<option value="image/png"> PNG </option>
						<option value="image/jpeg"> JPEG </option>
						<option value="image/gif"> GIF </option>
						<option value="image/x-icon"> X-Icon </option>
						<option value="image/x-jps"> JPS </option>
						<option value="image/pict"> PIC </option>
					</select>
				</div>
				<div className="db-buttons">
					<button className="button" onClick={this.refreshDatabase}> Scan Database </button>
					<button className="button danger" onClick={this.dropDatabase}> Drop Database </button>
				</div>
				<div className="search-bar">
					<input className="search-input" type="search" 
							onKeyDown={this.onSearchKeyDown} value={this.state.searchValue} 
							onChange={this.onSearchChange} placeholder="Search for folders, names, file endings.." />
					<button className="search-button" onClick={this.onSearchButtonClick}> Search </button>
				</div>
				<div className="page-buttons">
					<button className="prev-page-button" onClick={this.getPreviousPage}>{leftArrow}</button>
					<strong className="count"> {(this.props.page - 1) * this.props.limit + 1} - {this.props.page * this.props.limit} out of {this.props.count} </strong>
					<button className="next-page-button" onClick={this.getNextPage}>{rightArrow}</button>
				</div>
			</div>
		);
	}
}

export default Filters;
