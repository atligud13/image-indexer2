import React, { Component } from 'react';

class NoImagesFoundComponent extends Component {
	render() {
		return (
			<div className="no-img-found">
				<img className="cloud-img" src="https://cdn1.iconfinder.com/data/icons/hawcons/32/700435-icon-23-cloud-error-512.png" />
				<div> No images found </div>
			</div>
		);
	}
}

export default NoImagesFoundComponent;
