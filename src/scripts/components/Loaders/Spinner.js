import React, { Component } from 'react';

import '../../../styles/modules/Spinner.scss';

class Spinner extends Component {
	render() {
		let size = 120;
		if (this.props.size) size = this.props.size;
		return (
			<svg className="spinner" height={size + 'px'} width={size + 'px'} preserveAspectRatio="xMidYMid" viewBox="0 0 100 100">
				<circle cx="50" cy="50" fill="none" r="40" stroke="#90C3D4" strokeDasharray="163.36281798666926 87.9645943005142" strokeWidth="20"/>
			</svg>
		);
	}
}

export default Spinner;
